package inheritance.tablepersubclass;

import javax.persistence.*;

@Entity
@DiscriminatorValue("StudentType")
public class Student extends Person {
	private int year;
	
	public Student(String firstName, String lastName, int year) {
		super(firstName, lastName);
		this.year = year;
	}
	
	public Student() {};
	
	public String toString() {
		return firstName + " " + lastName + " " + year;
	}
}
