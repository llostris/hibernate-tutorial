package inheritance.tableperclasshierarchy;

import javax.persistence.Entity;

@Entity
public class Teacher extends Person {
	private String subject;
	
	public Teacher(String firstName, String lastName, String subject) {
		super(firstName, lastName);
		this.subject = subject;
	}
	
	public Teacher() {};
	
	public String toString() {
		return firstName + " " + lastName + " " + subject;
	}
}
