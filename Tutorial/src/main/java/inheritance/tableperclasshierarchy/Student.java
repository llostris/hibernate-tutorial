package inheritance.tableperclasshierarchy;

import javax.persistence.Entity;

@Entity
public class Student extends Person {
	private int year;
	
	public Student(String firstName, String lastName, int year) {
		super(firstName, lastName);
		this.year = year;
	}
	
	public Student() {};
	
	public String toString() {
		return firstName + " " + lastName + " " + year;
	}
}
