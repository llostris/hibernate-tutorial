package inheritance.tableperclasshierarchy;

import main.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class Main {

	public static void main(String[] args) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory("tableperclasshierarchy.cfg.xml");
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Student student1 = new Student("Ala", "Makota", 1);
		Teacher teacher1 = new Teacher("Alicja", "Alicjowska", "Matematyka");

		session.save(student1);
		session.save(teacher1);

		transaction.commit();
		session.close();

		session = sessionFactory.openSession();
		transaction = session.beginTransaction();

		long id = 1;
		student1 = (Student) session.get(Student.class, id);
		id = 2;
		teacher1 = (Teacher) session.get(Teacher.class, id);

		transaction.commit();
		session.close();

		System.out.println(student1);
		System.out.println(teacher1);

	}

}
