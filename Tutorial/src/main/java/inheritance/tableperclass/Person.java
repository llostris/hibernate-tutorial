package inheritance.tableperclass;

import javax.persistence.*;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@DiscriminatorValue("PersonType")
public abstract class Person {
	
	@Id
	@GeneratedValue
	@Column(name="person_id")
	private long id;
	protected String firstName;
	protected String lastName;
	
	public Person(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public Person() {
	};
}
