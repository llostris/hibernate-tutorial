package main;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;


public class HibernateUtil {

    private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry = null;

    private static SessionFactory buildSessionFactory(String configurationFile) {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            Configuration configuration = new Configuration();
            if ( configurationFile == null )
            	configuration.configure();
            else
            	configuration.configure(configurationFile);
            serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                    configuration.getProperties()).build();

            return configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
    	sessionFactory = buildSessionFactory(null);
        return sessionFactory;
    }
    
    public static SessionFactory getSessionFactory(String configurationFile) {
    	sessionFactory = buildSessionFactory(configurationFile);
        return sessionFactory;
        
    }

}