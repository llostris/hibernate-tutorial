import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/* 
 * Umozliwia sprawdzenie, czy stworzony przez nas plik konfiguracyjny hibernate.cfg.xml jest poprawny 
 * i czy mozliwe jest polaczenie z baza danych.
 * */

public class ConfigTest {
	
	
	public static void main(String [] args){
	    System.out.println("Hello hibernate");
	    
	    /* Dla Hibernate < 4.0 */
//	    SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
	    
	    /* Dla Hiberante >= 4.0 */
	    Configuration configuration = new Configuration();
	    configuration.configure();
	    ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
	            configuration.getProperties()).build();
	    SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	   
	    /* Otwieramy i zamykamy sesje */
	    Session session = sessionFactory.openSession();
	    session.close();
	    System.out.println("Plik konfiguracyjny jest poprawny.");
	}
}
